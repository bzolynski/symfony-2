<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Product;

class MainController extends Controller
{
    public function helloAction($name)
    {
        if ($name === 'Jim') {
            throw $this->createNotFoundException("I don't want say hello to Jim!");
        }

        $url = $this->generateUrl(
            'hello',
            array('name' => 'Jim'),
            true
        );

        return $this->render(
            'AppBundle:hello:index.html.twig',
            array('name' => $name, 'url' => $url)
        );

    }

    public function helloJsonAction($name) {

        // create a simple Response with a 200 status code (the default)
        $response = new Response('Hello '.$name, Response::HTTP_OK);

        // create a JSON-response with a 200 status code
        $response = new Response(json_encode(array('name' => $name)));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    public function createProductAction() {

        $product = new Product();
        $product->setName('Computer');
        $product->setPrice(1024);
        $product->setDescription('Rocket science computer!');

        $em = $this->getDoctrine()->getManager();

        $em->persist($product);
        $em->flush();

        return new Response('Product created with id: ' . $product->getId());
    }

    public function showProductAction($id) {

        $repository = $this->getDoctrine()->getRepository('AppBundle:Product');

        $product = $repository->find($id);

        if (!$product instanceof Product) {
            return new Response('Product with id: ' . $id . ' not found.');
        }

        return new Response(
            'Data for Product with id: ' . $id .
            '<br>Name: ' . $product->getName() .
            '<br>Price: ' . $product->getPrice() .
            '<br>Description: ' . $product->getDescription()
        );

    }

    public function showCategoriesAction() {
        $repository = $this->getDoctrine()->getRepository('AppBundle:Category');

        $categories = $repository->findAll();

        return $this->render(
            'AppBundle:Categories:index.html.twig',
            array('categories' => $categories)
        );

    }

}
