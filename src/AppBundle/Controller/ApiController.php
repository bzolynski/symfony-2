<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpKernel\Exception\HttpException;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\JsonResponse;

use AppBundle\Entity\Product;
use AppBundle\Repository\ProductRepository;

class ApiController extends FOSRestController
{

    /**
     * Array with HTTP codes which should throw exception.
     * @var array
     */
    private $errorCodes = array(
        Response::HTTP_NOT_FOUND,
        Response::HTTP_BAD_GATEWAY,
        Response::HTTP_BAD_REQUEST,
        Response::HTTP_FORBIDDEN,
        Response::HTTP_UNAUTHORIZED,
    );

    /**
     * Get products list by category
     *
     * @Rest\Get("/products/category/{categoryId}", name="",
     *      requirements={
     *          "categoryId"="\d+"
     *      }
     * )
     * @ApiDoc(
     *      section="Products"
     * )
     * @param int $categoryId
     * @return string
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     *
     * @Rest\View( serializerGroups = {"products"})
     */
    public function showProductsByCategoryAction($categoryId) {

        /** @var ProductRepository $repository */
        $repository = $this->getDoctrine()->getRepository('AppBundle:Product');

        //This result will be retrieved in two queries (lazy-loading): first for product and second for category (no matter fetch="EAGER" is set on relation)
        //$products = $repository->findByCategory($categoryId);

        //This result will be retrieved in single query thanks to joining category
        $products = $repository->getByCategory($categoryId);

        return $this->response(Response::HTTP_OK, "Products", array(
            'data' => $products
        ));
    }

    /**
     * @param int $httpCode
     * @param mixed $message
     * @param array $data
     * @return array
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     */
    public function response($httpCode = Response::HTTP_OK, $message = null, array $data = array())
    {
        $defaultArray = array(
            'status' => $httpCode,
        );

        if (!empty($message)) {
            $defaultArray['message'] = $message;
        }

        if (in_array($httpCode, $this->errorCodes)) {
            throw new HttpException($httpCode, $message);
        }

        return array_merge($defaultArray, $data);
    }
}