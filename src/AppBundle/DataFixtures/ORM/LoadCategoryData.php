<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Category;

class LoadGroupData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager) {

        $group = new Category();
        $group->setName('Desktops');
        $manager->persist($group);
        $this->addReference('group-desktops', $group);

        $group = new Category();
        $group->setName('Mobile');
        $manager->persist($group);
        $this->addReference('group-mobile', $group);

        $manager->flush();

    }

    public function getOrder() {
        return 1;
    }
}
