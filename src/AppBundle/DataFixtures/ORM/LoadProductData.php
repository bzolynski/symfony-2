<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Hardware;

class LoadProductData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager) {

        $product = new Hardware();
        $product->setName('Computer');
        $product->setPrice(1024);
        $product->setDescription('Rocket science computer!');
        $product->setCategory($this->getReference('group-desktops'));
        $product->setWeight(5000);
        $manager->persist($product);

        $product = new Hardware();
        $product->setName('Laptop');
        $product->setPrice(1092);
        $product->setDescription('Laptop from your dreams...');
        $product->setCategory($this->getReference('group-mobile'));
        $product->setWeight(2000);
        $manager->persist($product);

        $product = new Hardware();
        $product->setName('Phone');
        $product->setPrice(464);
        $product->setDescription('Just a phone, that\'s it');
        $product->setCategory($this->getReference('group-mobile'));
        $product->setWeight(150);
        $manager->persist($product);

        $manager->flush();

    }

    public function getOrder() {
        return 2;
    }
}
